# -*- CPERL -*-
use strict;
use LaTeXML::Package;

package LaTeXML::Package::Pool;

LoadClass('article');
RequirePackage('graphicx');
RequirePackage('color');

# Define a Beamer Overlay Parameter type
DefParameterType(
    'BeamerOverlay',
    sub {
        my ($gullet) = @_;
        my $tok = $gullet->readXToken;
        if ( ref $tok && ToString($tok) eq '<' ) {
            $gullet->readUntil( T_OTHER('>') );
        }
        else {
            $gullet->unread($tok) if ref $tok;
            undef;
        }
    },
    reversion => sub {
        ( T_OTHER('<'), $_[0]->revert, T_OTHER('>') );
    }
);

Tag('ltx:slide', autoClose => 1);
DefEnvironment( '{frame}[]', "<ltx:slide type=\"#1\">\r\n#body\n</ltx:slide>\n\n" );
#DefConstructor('\frame{}', '<ltx:slide>#1</ltx:slide>', mode => 'text',
#	bounded => 1, beforeDigest=>sub { reenterTextMode(); });
DefConstructor('\frame', '<ltx:slide>', mode => 'text');

Tag( 'frametitle' );
DefConstructor( '\frametitle{}', '<frametitle>#1</frametitle>' );

Tag('block');
DefEnvironment( '{block}', '<block>#body</block>' );
DefEnvironment( '{block}{}', '<block><title>#1</title>#body</block>' );

# Using tags breaks itemization
DefEnvironment('{columns}','#body');

Tag('column', autoClose => 1);
DefConstructor('\column{}','<column width="#1"/>');
#DefEnvironment('{column}{}','#body');
#DefEnvironment( '{column}{}', '<column width="#1">#body</column>' );


DefMacro( '\setbeamercovered {}',  '' );
DefMacro( '\setbeamertemplate {}', '' );
DefMacro( '\usetheme {}',          '' );
DefMacro( '\usecolortheme {}',     '' );
DefMacro( '\useoutertheme {}',     '' );

DefMacro( '\textwidth', '' );

Tag( "pause", autoClose => 1 );
DefConstructor( '\pause', '<pause/>' );

DefConstructor( '\mode OptionalBeamerOverlay {}', '<mode arg="#1">#2</mode>' );
DefConstructor( '\only OptionalBeamerOverlay {}', '<only arg="#1">#2</only>' );

Tag( 'titlepage', autoClose => 1 );
DefConstructor( '\titlepage', "<titlepage />" );
#Tag( 'tableofcontents', autoClose => 1);
#DefConstructor( '\tableofcontents', '<tableofcontents />');

DefMacroI( '\shorttitle',   undef, Tokens() );
DefMacroI( '\shortauthor',  undef, Tokens() );
DefMacroI( '\authors',      undef, Tokens() );
DefMacroI( '\shortauthors', undef, Tokens() );
DefMacroI( '\addresses',    undef, Tokens() );
DefMacroI( '\publname',     undef, Tokens() );
DefMacroI( '\publname',     undef, Tokens() );

DefMacro( '\captionsenglish', '\captions' );
DefMacro( '\dateenglish',     '\date' );
DefMacro( '\extrasenglish',   '\extras' );

DefConstructor( '\captions', '<captions/>' );
DefConstructor( '\extras',   '<extras/>' );

DefMacro( '\title[]{}',
    '\if.#1.\else\def\shorttitle{#1}\@add@frontmatter{ltx:toctitle}{#1}\fi'
        . '\@add@frontmatter{ltx:title}{#2}' );
DefMacro( '\author[]{}',
          '\if.#1.\else\def\shortauthor{#1}\fi'
        . '\@add@frontmatter{ltx:creator}[role=author]{\@personname{#2}}' );

RawTeX(<< 'EOTeX');
\definecolor{brown}{rgb}{0.59, 0.29, 0.0}
EOTeX

1;
